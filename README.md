# simplywik

Lets you write wikis in Markdown.

## Installation

`pip3 install --user simplywik`

## Usage

In your desired directory, run `simplywik init` to generate a new configuration file. An example is shown:

```yaml
clear_output_dir_on_build: true
data_directory: ./data
output_directory: ./build
```

- `clear_output_dir_on_build` removes the `output_directory` before building again.
- `data_directory` is the place where you store your markdown files.
- `output_directory` is the place where built HTML files are placed.

Once configured (and once your Markdown files are placed), run `simplywik build` to build the wiki.

## Supported Markdown

All markdown (with the exeption of *inline* footnotes; regular footnotes are supported) is supported.
