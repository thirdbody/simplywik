from typing import Dict
from colorama import Fore


def verify_config(config: dict) -> None:
    """Checks the provided config dictionary to ensure it contains the correct keys and all values are strings.
    Returns a dictionary containing an offending key/value pair, with the key unchanged and the value representing the needed remedy.
    If the returned dictionary is empty, the provided config is valid.
    """
    expected = {"data_directory": str, "output_directory": str, "clear_output_dir_on_build": bool}
    output = {}
    for element in expected.keys():
        if element not in config.keys():
            output[element] = "Not specified in config file"

    for key, val in config.items():
        if type(val) is not expected[key]:
            output[key] = "Incorrect type; change to {}".format(expected[key])

    if output:
        print(Fore.RED + "The specified config file contains errors!")
        for key, value in output.items():
            print("{}: {}".format(key, value))
        exit(1)

def create_config():
    return {"data_directory": "./data", "output_directory": "./build", "clear_output_dir_on_build": True}
