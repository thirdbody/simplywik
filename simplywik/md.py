import markdown
from colorama import Fore


def buildMarkdown(input_path: str, output_path: str) -> None:
    with open(input_path, "r") as md:
        with open(output_path, "w") as html:
            html.writelines(markdown.markdown(
                md.read(), extensions=['footnotes']))

    print(Fore.BLUE + "Processed {} -> {}".format(input_path, output_path))
